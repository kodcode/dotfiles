call plug#begin(expand('/home/kodcode/.vim/plugged'))
	Plug 'arcticicestudio/nord-vim'
	Plug 'preservim/nerdtree'
call plug#end()

syntax enable
set nocompatible
set background=dark
let g:solarized_termtrans = 1
colorscheme solarized
filetype on
filetype plugin on
filetype indent on
set number
set cursorline
set incsearch
set incsearch
set ignorecase
set smartcase
set showcmd
set showmode
set showmatch
set hlsearch
set history=50
set wildmenu
set wildmode=list:longest
set wildignore=*.docx,*.jpg,*.png,*.gif,*.pdf,*.pyc,*.exe,*.flv,*.img,*.xlsx

set statusline=
set statusline+=\ %F\ %M\ %Y\ %R
set statusline+=%=
set statusline+=\ ascii:\ %b\ hex:\ 0x%B\ row:\ %l\ col:\ %c\ percent:\ %p%%
set laststatus=2

set colorcolumn=80

