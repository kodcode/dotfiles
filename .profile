# $OpenBSD: dot.profile,v 1.8 2022/08/10 07:40:37 tb Exp $
#
# sh/ksh initialization

# https://www.ibm.com/support/pages/colorizing-csh-or-ksh-shell
export magenta="${e}[35m"
export yellow="${e}[33m"
export end="${e}[0m"
export LOGNAME=$(whoami)
export HOSTNAME=$(hostname)
export PS1="${yellow}\u@\h ${magenta}\w${end} \\$ "
export PICO_SDK_PATH=/home/kodcode/pico/pico-sdk

PATH=$HOME/bin:/bin:/sbin:/usr/bin:/usr/sbin:/usr/X11R6/bin:/usr/local/bin:/usr/local/sbin
export PATH HOME TERM
