#!/bin/sh

# Create a list of installed packages
pkg_info -mz | tee ~/dotfiles/installed_packages_list
# to reinstall the same packages run `pkg_add -l installed_packages_list`

# Copying files
cp ~/.mutt/muttrc ~/dotfiles/.mutt/
cp ~/.mutt/mutt-clear-dark.muttrc ~/dotfiles/.mutt/
cp ~/.mutt/mutt-clear-light.muttrc ~/dotfiles/.mutt/
cp ~/.mutt/mutt-colors-solarized-dark-16.muttrc ~/dotfiles/.mutt/
cp ~/.tmux.conf* ~/dotfiles/
cp ~/.vimrc* ~/dotfiles/
cp ~/.Xresources* ~/dotfiles/
cp ~/.xsession ~/dotfiles/
cp ~/.cwmrc ~/dotfiles/
cp -r ~/bin/* ~/dotfiles/bin/
cp ~/.profile ~/dotfiles/
cp ~/.lynxrc ~/dotfiles/
cp ~/.config/i3/config ~/dotfiles/.config/i3/

# pushing to remote repo
current_date_time=$(date)
cd ~/dotfiles/
git add .
git commit -m "$(date)"
git push
