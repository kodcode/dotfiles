#!/bin/ksh

cc -Wall -Wextra -fsanitize=address,undefined -Og -g3 -pedantic "$1"

